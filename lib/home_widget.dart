import 'package:flutter/material.dart';
import 'package:simhe_app/app_localization.dart';
import 'package:simhe_app/finland_widget.dart';
import 'package:simhe_app/finnish_widget.dart';
import 'package:simhe_app/profile.dart';
import 'package:simhe_app/profile_widget.dart';
import 'package:simhe_app/test_widget.dart';
import 'package:simhe_app/search_widget.dart';

import 'instructions_widget.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;

  List<GlobalKey<NavigatorState>> _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  //final _children = [_buildOffstageNavigator(0), _buildOffstageNavigator(1), _buildOffstageNavigator(2), _buildOffstageNavigator(3), _buildOffstageNavigator(4)];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          final isFirstRouteInCurrentTab =
              !await _navigatorKeys[_currentIndex].currentState.maybePop();

          // let system handle back button if we're on the first route
          return isFirstRouteInCurrentTab;
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text('SIMHE'),
            actions: [
              IconButton(
                icon: Icon(Icons.info),
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Instructions()));
                },
              )
            ],
          ),
          body: IndexedStack(index: _currentIndex, children: [
            _buildOffstageNavigator(0),
            _buildOffstageNavigator(1),
            _buildOffstageNavigator(2),
            _buildOffstageNavigator(3)
          ]),
          bottomNavigationBar: BottomNavigationBar(
            onTap: onTabTapped, // new
            currentIndex: _currentIndex, // new
            items: [
              new BottomNavigationBarItem(
                  icon: Icon(Icons.search),
                  label: AppLocalizations.of(context).translate('search')),
              new BottomNavigationBarItem(
                  icon: Icon(Icons.web),
                  label: AppLocalizations.of(context).translate('finland')),
              new BottomNavigationBarItem(
                  icon: Icon(Icons.web),
                  label: AppLocalizations.of(context).translate('finnish')),
              new BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  label: AppLocalizations.of(context).translate('profile')),
            ],
          ),
        ));
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, int index) {
    return {
      '/': (context) {
        return [Search(), FinlandWidget(), FinnishWidget(), Profile()]
            .elementAt(index);
      },
    };
  }

  Widget _buildOffstageNavigator(int index) {
    var routeBuilders = _routeBuilders(context, index);

    return Offstage(
      offstage: _currentIndex != index,
      child: Navigator(
        key: _navigatorKeys[index],
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (context) => routeBuilders[routeSettings.name](context),
          );
        },
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
