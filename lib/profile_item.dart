import 'package:flutter/material.dart';

class ProfileItem {

  bool isExpanded;
  final String header;
  final Widget body;


  ProfileItem(this.isExpanded, this.header, this.body);
}