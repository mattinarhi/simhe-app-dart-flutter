import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
part 'user.g.dart';



@HiveType(typeId: 1)
class User extends HiveObject {




  @HiveField(0)
  String firstName;

  @HiveField(1)
  String lastName;

  @HiveField(2)
  String dob;

  @HiveField(3)
  String telephone;

  @HiveField(4)
  String email;

  @HiveField(5)
  String address;

  @HiveField(6)
  String postalCode;

  @HiveField(7)
  String city;





  User();






  

}