import 'dart:io';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simhe_app/picture_widget.dart';
import 'package:simhe_app/profile_widget.dart';
import 'package:simhe_app/user_education.dart';
import 'package:simhe_app/user_work.dart';
import 'package:simhe_app/validation.dart';
import 'package:simhe_app/user.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';




class ProfileWidget extends StatefulWidget {
  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {

  final _validator = Validator();

  final _formKey = GlobalKey<FormState>();

  final _user = User();

  final _education = UserEducation();

  final _pictures = PictureWidget();

  final _employment = UserWork();


  
  @override
  void dispose() async {

    Hive.close();
    super.dispose();
  }



      sharePdfFile() async {

        final directory = await getTemporaryDirectory();

       Share.shareFiles(['${directory.path}/cv.pdf'], text: 'Your CV');
    


     }

     pw.Widget buildTextRow(String label, String profileValue) {

       return pw.Row(children: [pw.Expanded(flex: 1, child: pw.Text(label)),
       pw.Expanded(flex: 3, child: pw.Text(profileValue))]);


     }



  _printPDF() {
    Printing.layoutPdf(onLayout: (PdfPageFormat format) async {
      final doc = pw.Document();

      SharedPreferences prefs = await SharedPreferences.getInstance();

      final userBox = Hive.box<User>('userBox');

      final education = Hive.box<UserEducation>('education');

      final work = Hive.box<UserWork>('employment');

      String path = prefs.getString('user_image');


      final PdfImage profileImage = await pdfImageFromImageProvider(pdf: doc.document, image: FileImage(File('$path')));
    

  

      doc.addPage(pw.MultiPage(
          pageFormat: PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
          build: (pw.Context context) {
           return <pw.Widget>[
              
              pw.Column(children:<pw.Widget>[pw.Row(crossAxisAlignment: pw.CrossAxisAlignment.start,
                          mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                children:<pw.Widget>[pw.Header(level:0, child: pw.Text('CV')),
              pw.ClipOval(
                          child: pw.Container(
                            width: 200,
                            height: 100,
                            
                            child: pw.Image(profileImage)))]), pw.Header(level: 1, child: pw.Text("Personal Information")),
                            pw.Column(children:[buildTextRow('First Name', userBox.getAt(0).firstName)

                            
                              , buildTextRow('Last Name', userBox.getAt(0).lastName == null ? '' : userBox.getAt(0).lastName),
                              buildTextRow('Date of birth', userBox.getAt(0).dob),
                              buildTextRow('Tel.', userBox.getAt(0).telephone),
                              buildTextRow('Email', userBox.getAt(0).email),
                              buildTextRow('Address', userBox.getAt(0).address),
                              buildTextRow('Postal code', userBox.getAt(0).postalCode),
                              buildTextRow('City', userBox.getAt(0).city)
                              ]),

                              
                              pw.Header(level: 1, child: pw.Text(education.values.isEmpty == true ? '' : 'Education')),

                              pw.Column(children: [buildTextRow('Degree', education.getAt(0).degree == null ? '' : education.getAt(0).degree),
                              
                              buildTextRow('Degree Major', education.getAt(0).degreeMajor == null ? '' : education.getAt(0).degreeMajor),
                              buildTextRow('School', education.getAt(0).school == null ? '' : education.getAt(0).school),
                              buildTextRow('GPA', education.getAt(0).gpa == null ? '' : education.getAt(0).gpa),
                              buildTextRow('Start year', education.getAt(0).startYear == null ? '' : education.getAt(0).startYear),
                              buildTextRow('Graduation year', education.getAt(0).endYear == null ? '' : education.getAt(0).endYear)
                              ]),
                              

                              pw.Header(level: 1, child: pw.Text(work.values.isEmpty == true ? '' : 'Employment History')),

                              pw.Column(children: [
                                buildTextRow('Employer', work.getAt(0).employer == null ? '' : work.getAt(0).employer),
                                buildTextRow('Location', work.getAt(0).location == null ? '' : work.getAt(0).location),
                                buildTextRow('Job title',work.getAt(0).jobTitle == null ? '' : work.getAt(0).jobTitle),
                                buildTextRow('Start date', work.getAt(0).startDate == null ? '' : work.getAt(0).startDate),
                                buildTextRow('End date', work.getAt(0).endDate == null ? '' : work.getAt(0).endDate)
                              ])
                          
                      
                          ], 
            
            )];

    }));


      final output = await getTemporaryDirectory();
      print(output);
      final file = File("${output.path}/cv.pdf");
      await file.writeAsBytes(doc.save());
      return doc.save();
    });
  }






  @override
  Widget build(BuildContext context) {

 return Scaffold(

        
        resizeToAvoidBottomInset: false,

        appBar: AppBar(actions:<Widget>[FlatButton(child: Text('Generate CV'), onPressed:() {

          _printPDF();


        }),
        
        FlatButton(child: Text('Share CV'), onPressed: () async {
          sharePdfFile();
        })]),
      
      body:Container(child:  SingleChildScrollView(child: Column(children: <Widget>[_pictures,



      ValueListenableBuilder(
        valueListenable: Hive.box<UserEducation>('education').listenable(),

        
        builder: (context, Box<UserEducation> educationBox, _) {

          


          if (educationBox.values.isEmpty) {
            
            print('no education information');
          }
            return
     
            
          ValueListenableBuilder(
        valueListenable: Hive.box<User>('userBox').listenable(),

        
        builder: (context, Box<User> box, _) {

           if (box.values.isEmpty) {
            
            print('no personal information');

        }

        


      return     
      ValueListenableBuilder(
        valueListenable: Hive.box<UserWork>('employment').listenable(),

        
        builder: (context, Box<UserWork> jobBox, _) {

          


          if (jobBox.values.isEmpty) {
            
            print('no employment information');
          }
          
           
 
 return Form(
      
      
      key: _formKey,

      onChanged: () {

        if(_formKey.currentState.validate()) {

          _formKey.currentState.save();
          box.put(1, _user);

          educationBox.put(2, _education);

          jobBox.put(3, _employment);

          box.getAt(0).save();

          educationBox.getAt(0).save();

          jobBox.getAt(0).save();

        
          }


      },
      
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(color: Colors.blue, child: ListTile(title: Text('Personal information'))),

          TextFormField(

            initialValue: box.get(1) == null ? '' : box.getAt(0).firstName,
            decoration: InputDecoration(labelText:'First name'),
            onSaved: (value) => setState(() => _user.firstName = value),
            validator: (value) =>_validator.textValidator(value)
          
            
          ),

          TextFormField(decoration: InputDecoration(labelText: 'Last name'),

          initialValue: box.get(1) == null ? '' : box.getAt(0).lastName,

          onSaved: (value) => setState(() => _user.lastName = value), 
          validator: (value) => _validator.textValidator(value)),

          TextFormField(decoration: InputDecoration(labelText: 'Date of birth'),
          validator: (value) => _validator.dobValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).dob,
          onSaved: (value) => setState(() => _user.dob = value),
          ),

          Row(children: [Expanded(flex:2, child:TextFormField(decoration: InputDecoration(labelText:'Phone number'),
          onSaved: (value) => setState(() => _user.telephone = value),
          validator: (value) => _validator.phoneNumberValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).telephone)), 
          
          Expanded(flex:2,child:TextFormField(decoration: InputDecoration(labelText:'E-mail'),
          onSaved: (value) => setState(() => _user.email = value),
          validator: (value) => _validator.emailValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).email))]),

          TextFormField(decoration: InputDecoration(labelText: 'Address'),
          validator: (value) => _validator.textValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).address,
          onSaved: (value) => setState(() => _user.address = value),
          ),

          Row(children: [Expanded(flex:2, child:TextFormField(decoration: InputDecoration(labelText:'Postal code'),
          onSaved: (value) => setState(() => _user.postalCode = value),
          validator: (value) => _validator.postalCodeValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).postalCode)), 
          
          Expanded(flex:2,child:TextFormField(decoration: InputDecoration(labelText:'City'), 
          initialValue: box.get(1) == null ? '' : box.getAt(0).city,
          onSaved: (value) => setState(() => _user.city = value),
          validator: (value) => _validator.textValidator(value)))]),

          Container(color: Colors.blue,child: ListTile(title: Text('Education'))),


          TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).degree,
            decoration: InputDecoration(labelText: 'Degree'), onSaved: (value) => setState(() => _education.degree = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).degreeMajor,
            decoration: InputDecoration(labelText: 'Degree Major'), onSaved: (value) => setState(() => _education.degreeMajor = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).school,
            decoration: InputDecoration(labelText: 'School'), onSaved: (value) => setState(() => _education.school = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).gpa,
            decoration: InputDecoration(labelText: 'GPA'),
            onSaved: (value) => setState(() => _education.gpa = value),
            validator: (value) => _validator.gpaValidator(value)),

            Row(children: [Expanded(flex: 2, child: TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).startYear,
            decoration: InputDecoration(labelText: 'Starting year'), onSaved: (value) => setState(() => _education.startYear = value),
            validator: (value) => _validator.yearValidator(value))),
            
            Expanded(flex: 2, child: TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).endYear,
            decoration: InputDecoration(labelText: 'Graduation year'), onSaved: (value) => setState(() => _education.endYear = value),
            validator: (value) => _validator.yearValidator(value)))]),

            Container(color: Colors.blue, child: ListTile(title: Text('Employment history'))),
            
            TextFormField(decoration: InputDecoration(labelText: 'Employer'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).employer,
            onSaved: (value) => setState(() => _employment.employer = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(decoration: InputDecoration(labelText: 'Location'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).location,
            onSaved: (value) => setState(() => _employment.location = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(decoration: InputDecoration(labelText: 'Job title'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).jobTitle,
            onSaved: (value) => setState(() => _employment.jobTitle = value),
            validator: (value) => _validator.inputValidator(value)),

            Row(children: [Expanded(flex: 2, child: TextFormField(decoration: InputDecoration(labelText: 'Start date'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).startDate,
            onSaved: (value) => setState(() => _employment.startDate = value),
            validator: (value) => _validator.workDateValidator(value)
            )),
            
            Expanded(flex: 2, child: TextFormField(decoration: InputDecoration(labelText: 'End date'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).endDate,
            onSaved: (value) => setState(() => _employment.endDate = value)))]
            ),


          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
           ),
            
               ]));});});})]))
        ));
      
      
       

  }
}