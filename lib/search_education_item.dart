class Education {
  final String id;
  final String name;
  final String lopName;
  final String type;
  final String credits;
  final String rawJson;

  Education(
      {this.id,
      this.name,
      this.lopName,
      this.type,
      this.credits,
      this.rawJson});

  factory Education.fromJson(Map<String, dynamic> json) {
    return Education(
      id: json['id'],
      name: json['name'],
      lopName: json['lopNames'][0],
      type: json['type'],
      credits: json['credits'],
      rawJson: json.toString(),
    );
  }

  @override
  String toString() {
    return "name: ${this.name}, school: ${this.lopName}\n" +
        "type: ${this.type}, credits: ${this.credits}";
  }
}
