import 'dart:io';

import 'package:flutter/material.dart';
import 'package:simhe_app/app_localization.dart';
import 'package:simhe_app/finnish_items.dart';
import 'package:simhe_app/webviewer.dart';



class FinnishWidget extends StatefulWidget {
  @override
  _FinnishWidgetState createState() => _FinnishWidgetState();
}

class _FinnishWidgetState extends State<FinnishWidget> {

  List<FinnishItem> items;

  @override 
  void didChangeDependencies() {

    super.didChangeDependencies();

     items = [new FinnishItem(AppLocalizations.of(context).translate('finnish_title'), [
     AppLocalizations.of(context).translate('finnish_sub_title1'), 
     AppLocalizations.of(context).translate('finnish_sub_title2'), AppLocalizations.of(context).translate('finnish_sub_title3'),
     AppLocalizations.of(context).translate('finnish_sub_title4'), AppLocalizations.of(context).translate('finnish_sub_title5'), 
     AppLocalizations.of(context).translate('finnish_sub_title6'),
     AppLocalizations.of(context).translate('finnish_sub_title7'), AppLocalizations.of(context).translate('finnish_sub_title8')
  ], [
     AppLocalizations.of(context).translate('finnish_link1'),
     AppLocalizations.of(context).translate('finnish_link2'),
     AppLocalizations.of(context).translate('finnish_link3'),
     AppLocalizations.of(context).translate('finnish_link4'),
     AppLocalizations.of(context).translate('finnish_link5'),
     AppLocalizations.of(context).translate('finnish_link6'),
     AppLocalizations.of(context).translate('finnish_link7'),
     AppLocalizations.of(context).translate('finnish_link8')
  ])];

  }

  @override
  void initState() {

    super.initState();

    
  }



  @override
  Widget build(BuildContext context) {

            return Scaffold(body: ListView.builder(itemCount: items.length,
        
        itemBuilder: (context, i) {

          return ExpansionTile(title: Text(items[i].title), 
          children: [Column(children:_buildContent(items[i]))]);


        }));


  }

     _buildContent(FinnishItem item) {

     List<Widget> contents = [];

     for(int i = 0;  i< item.contents.length; i++)

       contents.add(ListTile(title: Text(item.contents[i]), onTap: () {

         Navigator.of(context).push(
              
         MaterialPageRoute(builder: (context) => WebViewer(item.urls[i])));


      }

   

      
       
       ));

       return contents;

    


    
      
    
  }
}