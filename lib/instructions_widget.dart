import 'package:flutter/material.dart';
import 'package:simhe_app/instructions_items.dart';
import 'package:simhe_app/app_localization.dart';

class Instructions extends StatefulWidget {
  @override
  _InstructionsState createState() => _InstructionsState();
}

class _InstructionsState extends State<Instructions> {
  List<InstructionsItem> items = [
    new InstructionsItem(
        "instructions_search_title", "instructions_search_text"),
    new InstructionsItem(
        "instructions_finland_title", "instructions_finland_text"),
    new InstructionsItem(
        "instructions_finnish_title", "instructions_finnish_text"),
    new InstructionsItem(
        "instructions_profile_title", "instructions_profile_text"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Instructions')),
        body: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, i) {
              return ExpansionTile(
                  title: Text(
                      AppLocalizations.of(context).translate(items[i].title)),
                  children: [
                    Column(children: _buildContent(context, items[i]))
                  ]);
            }));
  }

  _buildContent(BuildContext context, InstructionsItem item) {
    List<Widget> content = [];

    content.add(Text(AppLocalizations.of(context).translate(item.content),
        style: TextStyle(fontSize: 18.0)));

    return content;
  }
}
