import 'package:hive/hive.dart';
part 'user_education.g.dart';

@HiveType(typeId: 2)
class UserEducation extends HiveObject {

  @HiveField(0)
  String degree;

  @HiveField(1)
  String degreeMajor;

  @HiveField(2)
  String school;

  @HiveField(3)
  String gpa;

  @HiveField(4)
  String startYear;

  @HiveField(5)
  String endYear;

UserEducation();



}