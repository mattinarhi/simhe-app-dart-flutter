import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:simhe_app/home_widget.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
import 'package:simhe_app/user.dart';
import 'package:simhe_app/user_education.dart';
import 'package:simhe_app/user_work.dart';

import 'app_localization.dart';

void main() async {
  final storage = FlutterSecureStorage();

  final String key1 = 'key1';
  final String key2 = 'key2';

  final String key3 = 'key3';

  await Hive.initFlutter();

  if (await storage.read(key: key1) == null ||
      await storage.read(key: key2) == null ||
      await storage.read(key: key3) == null) {
    print('Generating new keys');

    var value1 = Hive.generateSecureKey();

    var value2 = Hive.generateSecureKey();

    var value3 = Hive.generateSecureKey();

    storage.write(key: key1, value: String.fromCharCodes(value1));
    storage.write(key: key2, value: String.fromCharCodes(value2));
    storage.write(key: key3, value: String.fromCharCodes(value3));

    await openBoxes(value1, value2, value3);
  } else {
    print('Fetching existing keys');

    String value1 = await storage.read(key: key1);
    String value2 = await storage.read(key: key2);
    String value3 = await storage.read(key: key3);

    List<int> v1 = value1.codeUnits;

    List<int> v2 = value2.codeUnits;

    List<int> v3 = value3.codeUnits;

    await openBoxes(v1, v2, v3);
  }

  runApp(App());
}

openBoxes(var value1, var value2, var value3) async {
  Hive.registerAdapter(UserAdapter());
  await Hive.openBox<User>('userBox', encryptionKey: value1);

  Hive.registerAdapter(UserEducationAdapter());
  await Hive.openBox<UserEducation>('education', encryptionKey: value2);

  Hive.registerAdapter(UserWorkAdapter());
  await Hive.openBox<UserWork>('employment', encryptionKey: value3);
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext buildContext) {
    return MaterialApp(
      title: 'SIMHE',
      theme: ThemeData(
        primaryColor: Colors.blue,
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
            selectedLabelStyle: TextStyle(color: Colors.blue),
            unselectedLabelStyle: TextStyle(color: Colors.grey),
            selectedItemColor: Colors.blue,
            unselectedItemColor: Colors.grey,
            showSelectedLabels: true,
            showUnselectedLabels: true),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(
          brightness: Brightness.dark,
          cursorColor: Colors.white,
          inputDecorationTheme: InputDecorationTheme(
              labelStyle: TextStyle(color: Colors.white),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white))),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
              selectedLabelStyle: TextStyle(color: Colors.white),
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.grey,
              unselectedLabelStyle: TextStyle(color: Colors.grey),
              showSelectedLabels: true,
              showUnselectedLabels: true),
          iconTheme: IconThemeData(color: Colors.white)),
      supportedLocales: [
        Locale('en', ''),
        Locale('fi', ''),
      ],
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      home: Home(),
    );
  }
}
