import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';


class PictureWidget extends StatefulWidget {
  @override
  _PictureWidgetState createState() => _PictureWidgetState();
}

class _PictureWidgetState extends State<PictureWidget> {

  File _image;

  final picker = ImagePicker();




  @override
  void initState() {

    fetchImage();


    super.initState();

 


  }

  @override
  void dispose() {

    super.dispose();
  }


  Future<void> fetchImage() async {


    await load();
  }





save() async {




Directory appDocDir = await getApplicationDocumentsDirectory();
String appDocPath = appDocDir.path;

File newImage = File(_image.path);

final fileName = basename(_image.path);

final String fileExtension = extension(_image.path);

 newImage = await _image.copy('$appDocPath\'$fileName$fileExtension');

 if(_image != null) {
SharedPreferences prefs = await SharedPreferences.getInstance();

prefs.setString('user_image', newImage.path);


 setState(() => _image = newImage);


}



  }


  Future load() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(prefs.containsKey('user_image') && prefs.getString('user_image') != null) {

      setState(() {
        _image = File(prefs.getString('user_image'));
      });
      


    }



  }

  Future<File> compressImage(File file) async {

    final filePath = file.absolute.path;
    

    final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";

    final compressedImage = await FlutterImageCompress.compressAndGetFile(
          filePath, 
          outPath,
          minWidth: 1000,
          minHeight: 1000,
          quality: 70);
    
    return compressedImage;
}
 



  



  _imgFromCamera() async {
  final image = await picker.getImage(
    source: ImageSource.camera, imageQuality: 50
  );

  final compressedImage = await compressImage(File(image.path));

  setState(() {
    if(compressedImage != null) {
    _image = File(compressedImage.path);

    }
    else {
      print('No image selected');
    }
    
  });

  save();
}

_imgFromGallery() async {
  final image = await  picker.getImage(
      source: ImageSource.gallery, imageQuality: 50
  );

  final compressedImage = await compressImage(File(image.path));

  setState(() {

    if(compressedImage != null) {
    _image = File(compressedImage.path);
    }
    else {

      print('No image selected');
    }
    
  });
  save();
}

void _showPicker(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Photo Library'),
                    onTap: () {
                      _imgFromGallery();
                      Navigator.of(context).pop();
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_camera),
                  title: new Text('Camera'),
                  onTap: () {
                    _imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ),
        );
      }
    );
}



  @override
  Widget build(BuildContext context) {
    return Container(child: Column(
      children: <Widget>[
        SizedBox(
          height: 32,
        ),
        Center(
          child: GestureDetector(
            onTap: () {
              _showPicker(context);
            },
            child: CircleAvatar(
              radius: 55,
              backgroundColor: Color(0xffFDCF09),
              child: _image != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: Image.file(
                        _image,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(50)),
                      width: 100,
                      height: 100,
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.grey[800],
                      ),
                    ),
            ),
          ),
        )
      ],
    ),
  );
}
      
    
  }
