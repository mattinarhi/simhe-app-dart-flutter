
class Validator {

String textValidator(String value) {

  if(value.isEmpty) {

    return 'Please insert information to this field ';


  }

  else if(value.length < 3) {

    return 'Too short input';
  }

  else {

    return null;
  }
}

String emailValidator(String value) {

  final RegExp regex = new RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");

  if(regex.hasMatch(value) == false) {

    return "Invalid e-mail address";


  }
    else {

      return null;
    }

}

String dobValidator(String value) {

  final RegExp regex = new RegExp(r"^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$");

  if(regex.hasMatch(value) == false) {

    return "Insert your Date of Birth";


  }

  else {

    return null;
  }

}

String phoneNumberValidator(String value) {

  final RegExp regex = new RegExp(r'\d\d\d\d\d\d\d\d\d\d$');

  if (regex.hasMatch(value) == false) {

    return "Invalid phone number";
  }

  else {

    return null;
  }


}

String postalCodeValidator(String value) {

  final RegExp regex = new RegExp(r'\d\d\d\d\d$');

  if(regex.hasMatch(value) == false) {

    return "Invalid postal code";

  }

  else {

    return null;
  }
}

String inputValidator(String value) {

  if(value.isEmpty) {

    return null;


  }

  else if(value.isEmpty == false && value.length < 3) {

    return "Invalid input";
  }
  else {
    return null;
  }
}

String yearValidator(String value) {

  final RegExp regExp = new RegExp(r'\d\d\d\d$');

  if(value.isEmpty) {

    return null;
  }

  else if(value.isEmpty == false && regExp.hasMatch(value) == false) {

    return "Invalid year input";


  }

  else {

    return null;
  }


}

String gpaValidator(String value) {

  final RegExp regExp1 = new RegExp(r'\d.\d\d?$');
  final RegExp regExp2 = new RegExp(r'\d,\d\d?$');


  if(value.isEmpty) {

    return null;
  }

  else if(value.isEmpty == false && (regExp1.hasMatch(value) == false && regExp2.hasMatch(value) == false)) {
    return "Invalid GPA input";
  }

  else {
    return null;
  }
}

String workDateValidator(String value) {

  final RegExp regExp = new RegExp(r'^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$');

  if(value.isEmpty) {

    return null;
  }

  else if(value.isEmpty == false && regExp.hasMatch(value) == false) {

    return "Invalid date input";
  }

  else {

    return null;
  }


}

}