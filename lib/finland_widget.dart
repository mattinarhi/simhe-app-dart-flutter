

import 'package:flutter/material.dart';
import 'package:simhe_app/app_localization.dart';

import 'package:simhe_app/finland_items.dart';

import 'package:simhe_app/webviewer.dart';
 
 




class FinlandWidget extends StatefulWidget {
  @override
  _FinlandWidgetState createState() => _FinlandWidgetState();
}

class _FinlandWidgetState extends State<FinlandWidget> {

  List<FinlandItem> items;
  


  @override

  void initState() {

  

    super.initState();

  }
  void didChangeDependencies () {

  super.didChangeDependencies();


  items = [new FinlandItem(AppLocalizations.of(context).translate('education_title'), [AppLocalizations.of(context).translate('education_sub_title1'),
  AppLocalizations.of(context).translate('education_sub_title2'), AppLocalizations.of(context).translate('education_sub_title3'), 
  AppLocalizations.of(context).translate('education_sub_title4'),
  AppLocalizations.of(context).translate('education_sub_title5'),
  AppLocalizations.of(context).translate('education_sub_title6'), AppLocalizations.of(context).translate('education_sub_title7'), 
  AppLocalizations.of(context).translate('education_sub_title8'),
  AppLocalizations.of(context).translate('education_sub_title9'), AppLocalizations.of(context).translate('education_sub_title10'), 
  AppLocalizations.of(context).translate('education_sub_title11')], 
  
  [AppLocalizations.of(context).translate('education_link1'),
  
   AppLocalizations.of(context).translate('education_link2'),
   AppLocalizations.of(context).translate('education_link3'),
   AppLocalizations.of(context).translate('education_link4'),
   AppLocalizations.of(context).translate('education_link5'),
   AppLocalizations.of(context).translate('education_link6'),
   AppLocalizations.of(context).translate('education_link7'),
   AppLocalizations.of(context).translate('education_link8'),
   AppLocalizations.of(context).translate('education_link9'),
   AppLocalizations.of(context).translate('education_link10'),
   AppLocalizations.of(context).translate('education_link11')]),

  new FinlandItem(AppLocalizations.of(context).translate('work_title'), [AppLocalizations.of(context).translate('work_sub_title1'), 
   AppLocalizations.of(context).translate('work_sub_title2'), 
   AppLocalizations.of(context).translate('work_sub_title3'),
   AppLocalizations.of(context).translate('work_sub_title4'), AppLocalizations.of(context).translate('work_sub_title5'), 
   AppLocalizations.of(context).translate('work_sub_title6'), AppLocalizations.of(context).translate('work_sub_title7'), 
   AppLocalizations.of(context).translate('work_sub_title8'),
   AppLocalizations.of(context).translate('work_sub_title9'), AppLocalizations.of(context).translate('work_sub_title10')], 
  
  [AppLocalizations.of(context).translate('work_link1'),
   AppLocalizations.of(context).translate('work_link2'),
   AppLocalizations.of(context).translate('work_link3'),
   AppLocalizations.of(context).translate('work_link4'),
   AppLocalizations.of(context).translate('work_link5'),
   AppLocalizations.of(context).translate('work_link6'),
   AppLocalizations.of(context).translate('work_link7'),
   AppLocalizations.of(context).translate('work_link8'),
   AppLocalizations.of(context).translate('work_link9'),
   AppLocalizations.of(context).translate('work_link10')]),

  new FinlandItem(AppLocalizations.of(context).translate('housing_title'), 
  [AppLocalizations.of(context).translate('housing_sub_title1'), 
   AppLocalizations.of(context).translate('housing_sub_title2'), AppLocalizations.of(context).translate('housing_sub_title3'), 
   AppLocalizations.of(context).translate('housing_sub_title4'), AppLocalizations.of(context).translate('housing_sub_title5'),
   AppLocalizations.of(context).translate('housing_sub_title6'), AppLocalizations.of(context).translate('housing_sub_title7'), 
   AppLocalizations.of(context).translate('housing_sub_title8'), AppLocalizations.of(context).translate('housing_sub_title9'), 
   AppLocalizations.of(context).translate('housing_sub_title10'),
   AppLocalizations.of(context).translate('housing_sub_title11'), AppLocalizations.of(context).translate('housing_sub_title12'), 
   AppLocalizations.of(context).translate('housing_sub_title13')], [
    
    
     AppLocalizations.of(context).translate('housing_link1'),
     AppLocalizations.of(context).translate('housing_link2'),
     AppLocalizations.of(context).translate('housing_link3'),
     AppLocalizations.of(context).translate('housing_link4'),
     AppLocalizations.of(context).translate('housing_link5'),
     AppLocalizations.of(context).translate('housing_link6'),
     AppLocalizations.of(context).translate('housing_link7'),
     AppLocalizations.of(context).translate('housing_link8'),
     AppLocalizations.of(context).translate('housing_link9'),
     AppLocalizations.of(context).translate('housing_link10'),
     AppLocalizations.of(context).translate('housing_link11'),
     AppLocalizations.of(context).translate('housing_link12'),
     AppLocalizations.of(context).translate('housing_link13')]),

    new FinlandItem(AppLocalizations.of(context).translate('society_title'), 
    [AppLocalizations.of(context).translate('society_sub_title1'), AppLocalizations.of(context).translate('society_sub_title2'), 
    AppLocalizations.of(context).translate('society_sub_title3'),
    AppLocalizations.of(context).translate('society_sub_title4'), AppLocalizations.of(context).translate('society_sub_title5')], [
      
      
       AppLocalizations.of(context).translate('society_link1'),
       AppLocalizations.of(context).translate('society_link2'),
       AppLocalizations.of(context).translate('society_link3'),
       AppLocalizations.of(context).translate('society_link4'),
       AppLocalizations.of(context).translate('society_link5')])
  
  ];

  
  }






  @override
  Widget build(BuildContext context) {

        return Scaffold(body: ListView.builder(itemCount: items.length,
        
        itemBuilder: (context, i) {

          return ExpansionTile(title: Text(items[i].title), 
          children: [Column(children:_buildContent(items[i]))]);


        })



   

          
 


          


         );


  }

   _buildContent(FinlandItem item) {

     List<Widget> contents = [];

     for(int i = 0;  i< item.contents.length; i++)

       contents.add(ListTile(title: Text(item.contents[i]), onTap: () {

         Navigator.of(context).push(
              
         MaterialPageRoute(builder: (context) => WebViewer(item.urls[i])));


      }

   

      
       
       ));

       return contents;

     

  



     }





    

  



  
    
   
      
  
      
    







     


  }
