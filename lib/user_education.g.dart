// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_education.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserEducationAdapter extends TypeAdapter<UserEducation> {
  @override
  final int typeId = 2;

  @override
  UserEducation read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserEducation()
      ..degree = fields[0] as String
      ..degreeMajor = fields[1] as String
      ..school = fields[2] as String
      ..gpa = fields[3] as String
      ..startYear = fields[4] as String
      ..endYear = fields[5] as String;
  }

  @override
  void write(BinaryWriter writer, UserEducation obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.degree)
      ..writeByte(1)
      ..write(obj.degreeMajor)
      ..writeByte(2)
      ..write(obj.school)
      ..writeByte(3)
      ..write(obj.gpa)
      ..writeByte(4)
      ..write(obj.startYear)
      ..writeByte(5)
      ..write(obj.endYear);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserEducationAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
