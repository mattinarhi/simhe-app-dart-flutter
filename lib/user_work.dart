

import 'package:hive/hive.dart';
part 'user_work.g.dart';

@HiveType(typeId: 3)
class UserWork extends HiveObject {


  @HiveField(0)
  String employer;

  @HiveField(1)
  String location;

  @HiveField(2)
  String jobTitle;

  @HiveField(3)
  String startDate;

  @HiveField(4)
  String endDate;

  UserWork();






}