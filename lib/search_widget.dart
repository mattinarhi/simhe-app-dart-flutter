import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:stream_transform/stream_transform.dart';
import 'package:simhe_app/search_education_item.dart';
import 'package:simhe_app/app_localization.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  StreamController<String> controller = StreamController();
  List<Education> educations = List<Education>();

  // Fetch educations from opintopolku.fi using specified search terms
  Future<List<Education>> fetchEducations(searchText) async {
    final response = await http.get(
        AppLocalizations.of(context).translate('search_uri_1') +
            searchText +
            AppLocalizations.of(context).translate('search_uri_2'),
        headers: {"Caller-Id": "simhe2.simhe2"});

    List<Education> educations = [];
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      for (var j in jsonData['results']) {
        Education education = Education.fromJson(j);
        educations.add(education);
      }
    } else {
      print(response.statusCode);
      print(response.body.toString());
    }

    return educations;
  }

  @override
  void initState() {
    controller.stream.debounce(Duration(milliseconds: 400)).listen((text) =>
        fetchEducations(text)
            .then((response) => setState(() => educations = response)));
    super.initState();
  }

  @override
  void dispose() {
    controller.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Column(
      children: <Widget>[
        TextField(
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w600,
                fontFamily: "Arial"),
            onChanged: controller.add),
        Expanded(
            child: ListView.builder(
                itemCount: educations.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.symmetric(vertical: 4.0),
                    decoration: BoxDecoration(
                        border: Border(
                      bottom:
                          BorderSide(width: 2.0, color: Colors.grey.shade700),
                    )),
                    child: Text(educations[index].toString()),
                  );
                }))
      ],
    )));
  }
}
