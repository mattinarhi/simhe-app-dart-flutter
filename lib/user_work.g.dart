// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_work.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserWorkAdapter extends TypeAdapter<UserWork> {
  @override
  final int typeId = 3;

  @override
  UserWork read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserWork()
      ..employer = fields[0] as String
      ..location = fields[1] as String
      ..jobTitle = fields[2] as String
      ..startDate = fields[3] as String
      ..endDate = fields[4] as String;
  }

  @override
  void write(BinaryWriter writer, UserWork obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.employer)
      ..writeByte(1)
      ..write(obj.location)
      ..writeByte(2)
      ..write(obj.jobTitle)
      ..writeByte(3)
      ..write(obj.startDate)
      ..writeByte(4)
      ..write(obj.endDate);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserWorkAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
