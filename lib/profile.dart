import 'dart:io';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simhe_app/picture_widget.dart';
import 'package:simhe_app/profile_item.dart';
import 'package:simhe_app/user_education.dart';
import 'package:simhe_app/user_work.dart';
import 'package:simhe_app/validation.dart';
import 'package:simhe_app/user.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/intl.dart';



class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

    final _validator = Validator();

  final _formKey1 = GlobalKey<FormState>();

  final _formKey2 = GlobalKey<FormState>();

  final _formKey3 = GlobalKey<FormState>();

  final _user = User();

  final _education = UserEducation();

  final _secondEducation = UserEducation();

  final _pictures = PictureWidget();

  final _employment = UserWork();

  final _secondEmployment = UserWork();

  List<ProfileItem> items;

  


_ProfileState() {

     items = [new ProfileItem(false, 'Personal information', ValueListenableBuilder(
        valueListenable: Hive.box<User>('userBox').listenable(),

        
        builder: (context, Box<User> box, _) {

           if (box.values.isEmpty) {
            
            print('no personal information');

        }
        

        return  Form(

        key: _formKey1,

      onChanged: () {

        if(_formKey1.currentState.validate()) {

          _formKey1.currentState.save();
          box.put(1, _user);
          
        }

      },

      child: Column(children: [
                  TextFormField(

            initialValue: box.get(1) == null ? '' : box.getAt(0).firstName,
            decoration: InputDecoration(labelText:'First name'),
            onSaved: (value) => setState(() => _user.firstName = value),
            validator: (value) =>_validator.textValidator(value)
          
            
          ),

          TextFormField(decoration: InputDecoration(labelText: 'Last name'),

          initialValue: box.get(1) == null ? '' : box.getAt(0).lastName,

          onSaved: (value) => setState(() => _user.lastName = value), 
          validator: (value) => _validator.textValidator(value)),

          TextFormField(decoration: InputDecoration(labelText: 'Date of birth'),
          validator: (value) => _validator.dobValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).dob,
          onSaved: (value) => setState(() => _user.dob = value),
          ),

          Row(children: [Expanded(flex:2, child:TextFormField(decoration: InputDecoration(labelText:'Phone number'),
          onSaved: (value) => setState(() => _user.telephone = value),
          validator: (value) => _validator.phoneNumberValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).telephone)), 
          
          Expanded(flex:2,child:TextFormField(decoration: InputDecoration(labelText:'E-mail'),
          onSaved: (value) => setState(() => _user.email = value),
          validator: (value) => _validator.emailValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).email))]),

          TextFormField(decoration: InputDecoration(labelText: 'Address'),
          validator: (value) => _validator.textValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).address,
          onSaved: (value) => setState(() => _user.address = value),
          ),

          Row(children: [Expanded(flex:2, child:TextFormField(decoration: InputDecoration(labelText:'Postal code'),
          onSaved: (value) => setState(() => _user.postalCode = value),
          validator: (value) => _validator.postalCodeValidator(value),
          initialValue: box.get(1) == null ? '' : box.getAt(0).postalCode)), 
          
          Expanded(flex:2,child:TextFormField(decoration: InputDecoration(labelText:'City'), 
          initialValue: box.get(1) == null ? '' : box.getAt(0).city,
          onSaved: (value) => setState(() => _user.city = value),
          validator: (value) => _validator.textValidator(value)))])
      ]),




        );
        
        
        }))
        ,new ProfileItem(false,    'Education',   ValueListenableBuilder(
        valueListenable: Hive.box<UserEducation>('education').listenable(),

        
        builder: (context, Box<UserEducation> educationBox, _) {

          


          if (educationBox.values.isEmpty) {

            educationBox.put(2, _education);
            educationBox.put(4, _secondEducation);
            
            print('no education information');
          }
            return Form(key: _formKey2,
            onChanged: () {
              if(_formKey2.currentState.validate()) {

                _formKey2.currentState.save();
                educationBox.put(2, _education);
              
                educationBox.put(4, _secondEducation);
              
           
              }
            },
            child: Column(children: [
                        TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).degree,
            decoration: InputDecoration(labelText: 'Degree'), onSaved: (value) => setState(() => _education.degree = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).degreeMajor,
            decoration: InputDecoration(labelText: 'Degree Major'), onSaved: (value) => setState(() => _education.degreeMajor = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).school,
            decoration: InputDecoration(labelText: 'School'), onSaved: (value) => setState(() => _education.school = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).gpa,
            decoration: InputDecoration(labelText: 'GPA'),
            onSaved: (value) => setState(() => _education.gpa = value),
            validator: (value) => _validator.gpaValidator(value)),

            Row(children: [Expanded(flex: 2, child: TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).startYear,
            decoration: InputDecoration(labelText: 'Starting year'), onSaved: (value) => setState(() => _education.startYear = value),
            validator: (value) => _validator.yearValidator(value))),
            
            Expanded(flex: 2, child: TextFormField(initialValue: educationBox.get(2) == null ? '' : educationBox.getAt(0).endYear,
            decoration: InputDecoration(labelText: 'Graduation year'), onSaved: (value) => setState(() => _education.endYear = value),
            validator: (value) => _validator.yearValidator(value)))]),

            Container(color: Colors.blue, child: ListTile(title: Text('Second Education'))),


                        TextFormField(initialValue: educationBox.get(4)== null ? '' : educationBox.getAt(1).degree,
            decoration: InputDecoration(labelText: 'Degree'), onSaved: (value) => setState(() => _secondEducation.degree = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(4) == null ? '' : educationBox.getAt(1).degreeMajor,
            decoration: InputDecoration(labelText: 'Degree Major'), onSaved: (value) => setState(() => _secondEducation.degreeMajor = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(4) == null ? '' : educationBox.getAt(1).school,
            decoration: InputDecoration(labelText: 'School'), onSaved: (value) => setState(() => _secondEducation.school = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(initialValue: educationBox.get(4) == null ? '' : educationBox.getAt(1).gpa,
            decoration: InputDecoration(labelText: 'GPA'),
            onSaved: (value) => setState(() => _secondEducation.gpa = value),
            validator: (value) => _validator.gpaValidator(value)),

            Row(children: [Expanded(flex: 2, child: TextFormField(initialValue: educationBox.get(4) == null ? '' : educationBox.getAt(1).startYear,
            decoration: InputDecoration(labelText: 'Starting year'), onSaved: (value) => setState(() => _secondEducation.startYear = value),
            validator: (value) => _validator.yearValidator(value))),
            
            Expanded(flex: 2, child: TextFormField(initialValue: educationBox.get(4) == null ? '' : educationBox.getAt(1).endYear,
            decoration: InputDecoration(labelText: 'Graduation year'), onSaved: (value) => setState(() => _secondEducation.endYear = value),
            validator: (value) => _validator.yearValidator(value)))])





            ]));
            })
                    ),
                    
              new ProfileItem(false,  "Employment history",  ValueListenableBuilder(
        valueListenable: Hive.box<UserWork>('employment').listenable(),

        
        builder: (context, Box<UserWork> jobBox, _) {

          


          if (jobBox.values.isEmpty) {

            jobBox.put(3, _employment);
            jobBox.put(5, _secondEmployment);
            
            print('no employment information');
          }

          return Form(

            key: _formKey3,

            onChanged: () { 
              if(_formKey3.currentState.validate()) {

                _formKey3.currentState.save();
                jobBox.put(3, _employment);
                jobBox.put(5, _secondEmployment);
                
              }
            },

            child: Column(children: [
                          TextFormField(decoration: InputDecoration(labelText: 'Employer'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).employer,
            onSaved: (value) => setState(() => _employment.employer = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(decoration: InputDecoration(labelText: 'Location'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).location,
            onSaved: (value) => setState(() => _employment.location = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(decoration: InputDecoration(labelText: 'Job title'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).jobTitle,
            onSaved: (value) => setState(() => _employment.jobTitle = value),
            validator: (value) => _validator.inputValidator(value)),

            Row(children: [Expanded(flex: 2, child: TextFormField(decoration: InputDecoration(labelText: 'Start date'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).startDate,
            onSaved: (value) => setState(() => _employment.startDate = value),
            validator: (value) => _validator.workDateValidator(value)
            )),
            
            Expanded(flex: 2, child: TextFormField(decoration: InputDecoration(labelText: 'End date'),
            initialValue: jobBox.get(3) == null ? '' : jobBox.getAt(0).endDate,
            onSaved: (value) => setState(() => _employment.endDate = value)))]
            ),



            Container(color: Colors.blue, child: ListTile(title: Text('Second Job'))),



                                      TextFormField(decoration: InputDecoration(labelText: 'Employer'),
            initialValue: jobBox.get(5) == null ? '' : jobBox.getAt(1).employer,
            onSaved: (value) => setState(() => _secondEmployment.employer = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(decoration: InputDecoration(labelText: 'Location'),
            initialValue: jobBox.get(5) == null ? '' : jobBox.getAt(1).location,
            onSaved: (value) => setState(() => _secondEmployment.location = value),
            validator: (value) => _validator.inputValidator(value)),

            TextFormField(decoration: InputDecoration(labelText: 'Job title'),
            initialValue: jobBox.get(5) == null ? '' : jobBox.getAt(1).jobTitle,
            onSaved: (value) => setState(() => _secondEmployment.jobTitle = value),
            validator: (value) => _validator.inputValidator(value)),

            Row(children: [Expanded(flex: 2, child: TextFormField(decoration: InputDecoration(labelText: 'Start date'),
            initialValue: jobBox.get(5) == null ? '' : jobBox.getAt(1).startDate,
            onSaved: (value) => setState(() => _secondEmployment.startDate = value),
            validator: (value) => _validator.workDateValidator(value)
            )),
            
            Expanded(flex: 2, child: TextFormField(decoration: InputDecoration(labelText: 'End date'),
            initialValue: jobBox.get(5) == null ? '' : jobBox.getAt(1).endDate,
            onSaved: (value) => setState(() => _secondEmployment.endDate = value)))]
            )





            ]),


          );
    
    
        }))      
                    ];
          
}
  
  
  
  
  
  @override
  void dispose() async {

    Hive.close();
    super.dispose();
  }

  _buildEmployment() {

    List<pw.Widget> content = [];

    final work = Hive.box<UserWork>('employment');

    DateTime dateTime1;

    DateTime dateTime2;

    try {

     dateTime1 = DateFormat('dd.MM.yyyy').parse(work.getAt(0).endDate);

     dateTime2 = DateFormat('dd.MM.yyyy').parse(work.getAt(1).endDate);
    }
    on FormatException {

      dateTime1 = null;
      dateTime2 = null;
    }

    

    if((dateTime1 != null && dateTime2 != null)&&dateTime1.isBefore(dateTime2)) {

                              content.add(buildTextRow('Employer', work.getAt(1).employer == null ? '' : work.getAt(1).employer));
                              content.add(buildTextRow('Location', work.getAt(1).location == null ? '' : work.getAt(1).location));
                              content.add(buildTextRow('Job title',work.getAt(1).jobTitle == null ? '' : work.getAt(1).jobTitle));
                              content.add(buildTextRow('Start date', work.getAt(1).startDate == null ? '' : work.getAt(1).startDate));
                              content.add(buildTextRow('End date', work.getAt(1).endDate == null ? '' : work.getAt(1).endDate));

                              content.add(pw.Padding(padding: pw.EdgeInsets.all(16.0)));

                              content.add(buildTextRow('Employer', work.getAt(0).employer == null ? '' : work.getAt(0).employer));
                              content.add(buildTextRow('Location', work.getAt(0).location == null ? '' : work.getAt(0).location));
                              content.add(buildTextRow('Job title',work.getAt(0).jobTitle == null ? '' : work.getAt(0).jobTitle));
                              content.add(buildTextRow('Start date', work.getAt(0).startDate == null ? '' : work.getAt(0).startDate));
                              content.add(buildTextRow('End date', work.getAt(0).endDate == null ? '' : work.getAt(0).endDate));

  }

  else {

                              content.add(buildTextRow('Employer', work.getAt(0).employer == null ? '' : work.getAt(0).employer));
                              content.add(buildTextRow('Location', work.getAt(0).location == null ? '' : work.getAt(0).location));
                              content.add(buildTextRow('Job title',work.getAt(0).jobTitle == null ? '' : work.getAt(0).jobTitle));
                              content.add(buildTextRow('Start date', work.getAt(0).startDate == null ? '' : work.getAt(0).startDate));
                              content.add(buildTextRow('End date', work.getAt(0).endDate == null ? '' : work.getAt(0).endDate));

                              content.add(pw.Padding(padding: pw.EdgeInsets.all(16.0)));

                              content.add(buildTextRow('Employer', work.getAt(1).employer == null ? '' : work.getAt(1).employer));
                              content.add(buildTextRow('Location', work.getAt(1).location == null ? '' : work.getAt(1).location));
                              content.add(buildTextRow('Job title',work.getAt(1).jobTitle == null ? '' : work.getAt(1).jobTitle));
                              content.add(buildTextRow('Start date', work.getAt(1).startDate == null ? '' : work.getAt(1).startDate));
                              content.add(buildTextRow('End date', work.getAt(1).endDate == null ? '' : work.getAt(1).endDate));





  }

  return content;



  }


  _buildEducation() {

   List<pw.Widget> content = [];


    final education = Hive.box<UserEducation>('education');

    var year1 = int.tryParse(education.getAt(0).startYear == null ? '' : education.getAt(0).startYear);
    var year2 = int.tryParse(education.getAt(1).startYear == null ? '' : education.getAt(1).startYear);

    if ((year1 != null && year2 !=null) && year1 <= year2) {

    content.add (buildTextRow('Degree', education.getAt(1).degree == null ? '' : education.getAt(1).degree));
                              
   content.add(buildTextRow('Degree Major', education.getAt(1).degreeMajor == null ? '' : education.getAt(1).degreeMajor));
                              
   content.add(buildTextRow('School', education.getAt(1).school == null ? '' : education.getAt(1).school));
                              
   content.add(buildTextRow('GPA', education.getAt(1).gpa == null ? '' : education.getAt(1).gpa));
                              
   content.add(buildTextRow('Start year', education.getAt(1).startYear == null ? '' : education.getAt(1).startYear));
                              
  content.add(buildTextRow('Graduation year ', education.getAt(1).endYear == null ? '' : education.getAt(1).endYear)); 

content.add(pw.Padding(padding: pw.EdgeInsets.all(16.0)));

  content.add(    buildTextRow('Degree', education.getAt(0).degree == null ? '' : education.getAt(0).degree));
                              
 content.add(buildTextRow('Degree Major', education.getAt(0).degreeMajor == null ? '' : education.getAt(0).degreeMajor));
 content.add(buildTextRow('School', education.getAt(0).school == null ? '' : education.getAt(0).school));
 content.add(buildTextRow('GPA', education.getAt(0).gpa == null ? '' : education.getAt(0).gpa));
 content.add(buildTextRow('Start year', education.getAt(0).startYear == null ? '' : education.getAt(0).startYear));
 content.add(buildTextRow('Graduation year ', education.getAt(0).endYear == null ? '' : education.getAt(0).endYear));



    }
    
     else {

    content.add(buildTextRow('Degree', education.getAt(0).degree == null ? '' : education.getAt(0).degree));
                              
  content.add(buildTextRow('Degree Major', education.getAt(0).degreeMajor == null ? '' : education.getAt(0).degreeMajor));
   content.add(buildTextRow('School', education.getAt(0).school == null ? '' : education.getAt(0).school));
   content.add(buildTextRow('GPA', education.getAt(0).gpa == null ? '' : education.getAt(0).gpa));
   content.add(buildTextRow('Start year', education.getAt(0).startYear == null ? '' : education.getAt(0).startYear));
    content.add(buildTextRow('Graduation year ', education.getAt(0).endYear == null ? '' : education.getAt(0).endYear));

  content.add(pw.Padding(padding: pw.EdgeInsets.all(16.0)));

 content.add(buildTextRow('Degree', education.getAt(1).degree == null ? '' : education.getAt(1).degree));
                              
 content.add(buildTextRow('Degree Major', education.getAt(1).degreeMajor == null ? '' : education.getAt(1).degreeMajor));
                              
 content.add(buildTextRow('School', education.getAt(1).school == null ? '' : education.getAt(1).school));
                              
 content.add(buildTextRow('GPA', education.getAt(1).gpa == null ? '' : education.getAt(1).gpa));
                              
 content.add(buildTextRow('Start year', education.getAt(1).startYear == null ? '' : education.getAt(1).startYear));
                              
 content.add(buildTextRow('Graduation year ', education.getAt(1).endYear == null ? '' : education.getAt(1).endYear));









    }

    return content;



  }



     pw.Widget buildTextRow(String label, String profileValue) {

      if(profileValue != '') {
       return pw.Row(children: [pw.Expanded(flex: 1, child: pw.Text(label, style: pw.TextStyle(fontSize: 16.0))),
       pw.Expanded(flex: 3, child: pw.Text(profileValue, style: pw.TextStyle(fontSize: 16.0)))]);

      }
      else {

        return pw.Text('');
      }


     }



  _printPDF() async {
  
      final doc = pw.Document();

      SharedPreferences prefs = await SharedPreferences.getInstance();

      final userBox = Hive.box<User>('userBox');

    

      final work = Hive.box<UserWork>('employment');

      String path = prefs.getString('user_image');


      final PdfImage profileImage = await pdfImageFromImageProvider(pdf: doc.document, image: FileImage(File('$path')));
    

    

      doc.addPage(pw.MultiPage(
          pageFormat: PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
          build: (pw.Context context) {
           return <pw.Widget>[
              
              pw.Column(children:<pw.Widget>[pw.Row(crossAxisAlignment: pw.CrossAxisAlignment.start,
                          mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                children:<pw.Widget>[pw.Header(level:0, child: pw.Text('CV')),
              pw.ClipOval(
                          child: pw.Container(
                            width: 200,
                            height: 100,
                            
                            child: pw.Image(profileImage)))]), pw.Header(level: 1, child: pw.Text("Personal Information", style: pw.TextStyle(fontSize: 16.0))),
                            pw.Column(children:[buildTextRow('First Name', userBox.getAt(0).firstName)

                            
                              , buildTextRow('Last Name', userBox.getAt(0).lastName),
                              buildTextRow('Date of birth', userBox.getAt(0).dob),
                              buildTextRow('Tel.', userBox.getAt(0).telephone),
                              buildTextRow('Email', userBox.getAt(0).email),
                              buildTextRow('Address', userBox.getAt(0).address),
                              buildTextRow('Postal code', userBox.getAt(0).postalCode),
                              buildTextRow('City', userBox.getAt(0).city)
                              ]),


                              pw.Header(level: 1, child: pw.Text('Education', style: pw.TextStyle(fontSize: 16.0))),

                              pw.Column(children:   _buildEducation()      
                              



                              
                              ),
                              

                              pw.Header(level: 1, child: pw.Text('Employment History', style: pw.TextStyle(fontSize: 16.0))),

                              pw.Column(children: _buildEmployment()
                              
                              
                              
                              
                              
                              
                              )

                          
                      
                          ], 
            
            )];

    }));

      
        
  


      Navigator.push(context, MaterialPageRoute(builder: (context) => Scaffold(appBar: AppBar(), body:(PdfPreview(allowSharing: true, pdfFileName: 'cv.pdf', build: (format) => doc.save())))));

    
    

   
  }
  
  
  
  
  
  
  
  
  
  
  @override
  Widget build(BuildContext context) {


     return Scaffold(

        
        resizeToAvoidBottomInset: false,

        appBar: AppBar(actions:<Widget>[FlatButton(child: Text('Generate CV'), onPressed:() async {

  
     
      _printPDF();          


        }),
        
]),
      
      body:Container(child:  SingleChildScrollView(child: Column(children: <Widget>[_pictures,
      
       ExpansionPanelList(

        animationDuration: Duration(milliseconds: 500),
        
        expansionCallback: (int index, bool isExpanded) {

                        setState(() {
                items[index].isExpanded = !items[index].isExpanded;
              });

        },
        children: items.map((ProfileItem item) {
          return ExpansionPanel(canTapOnHeader: true,
          headerBuilder: (BuildContext context, bool isExpanded) {

            return ListTile(title: Text(item.header));


          },
          body: item.body,
          isExpanded: item.isExpanded     
          );

  

        }).toList(),
      ),

     ]))));
      



      
    
      
    
  }
    
  


}